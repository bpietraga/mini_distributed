defmodule Election do
  @moduledoc """
  The module responsible for election handling
  """

  # T
  @time 1_000
  # 4T
  @wait_time @time * 4

  @doc """
  Gets all nodes higher than itself, and checks with ALIVE? message
  """
  def start do
    IO.puts("Election start\n")

    if higher_working_nodes?() do
      IO.puts("Lost election: #{Node.self()}\n")

      # wait 4 x T if no IAMTHEKING message is received, start election again
      receive do
        {:IAMTHEKING, node} -> LeaderLogic.update_leader(node)
      after
        @wait_time -> LeaderLogic.start_election()
      end
    else
      IO.puts("New election: #{Node.self()}\n")
      LeaderLogic.update_leader(Node.self())
      broadcast_victory()
    end
  end

  @doc """
  Checks if any of nodes with higher id is ALIVE?
  """
  def higher_working_nodes? do
    higher_nodes() |> Enum.any?(fn node ->
      :global.send(node, {:ALIVE?, Node.self()})

      receive do
        {:FINETHANKS, remote} when remote == node -> true
      after
        @wait_time -> false
      end
    end)
  end

  @doc """
  Gets all nodes higher than itself
  """
  def higher_nodes do
    Node.list() |> Enum.filter(fn node ->
      NodeData.node_id(node) > NodeData.node_id(Node.self())
    end)
  end

  @doc """
  Sends a IAMTHEKING message for each connected node
  """
  def broadcast_victory() do
    Enum.each(Node.list(), fn node ->
      :global.send(node, {:IAMTHEKING, Node.self()})
    end)
  end
end
