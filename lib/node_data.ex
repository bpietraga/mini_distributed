defmodule NodeData do
  @moduledoc """
  The  module responsible for showing node data and handling id fething
  """

  @doc """
  Return list of sorted connected nodes
  """
  def running_nodes do
    Enum.sort([node() | Node.list()], &(node_id(&1) <= node_id(&2)))
  end

  @doc """
  Print the list of nodes
  """
  def print_nodes do
    IO.puts("List of available nodes:")

    Enum.each(
      running_nodes(),
      fn node -> IO.puts(to_string(node)) end
    )
  end

  @doc """
  Returns id of given node as Integer
  """
  def node_id(node) do
    to_string(node)
    |> fetch_id
  end

  @doc """
  Return integer id of the nodes
  """
  def fetch_id(node) do
    [node_id | _host] = String.split(node, "@")
    node_id |> Integer.parse()
  end
end
