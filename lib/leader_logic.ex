defmodule LeaderLogic do
  @moduledoc """
  The module responsible for setting leader of the nodes
  """

  def initialize_leader(leader) do
    Agent.start_link(fn -> leader end, name: :leader)
  end

  def current_leader do
    Agent.get(:leader, & &1)
  end

  def update_leader(new_leader) do
    Agent.update(:leader, fn _ -> new_leader end)
  end
end
