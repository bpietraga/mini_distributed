defmodule MiniDistributed do
  @moduledoc """
  The main module responsible for the spawning
  """

  # T
  @time 1_000
  # 4T
  @wait_time @time * 4

  def init do
    # Start leader as itself
    LeaderLogic.initialize_leader(Node.self())

    # Get nodes status messages
    :global_group.monitor_nodes(true)
    :global.register_name(Node.self(), self())
  end

  @doc """
  Call this to start a node by itself
  """
  def start do
    init()
    loop()
  end

  @doc """
  Call this to start a node and connect to an existing node
  """
  def connect(id, hostname \\ host()) do
    init()
    # Connect to other nodes
    (to_string(id) <> "@" <> hostname)
    |> String.to_atom()
    |> Node.connect()

    :global.sync()
    loop()
  end

  @doc """
  Gets machine hostname
  """
  def host do
    :inet.gethostname()
    |> elem(1)
    |> to_string
  end


  @doc """
  Listening loop waiting for events with sleep T variable as @time
  """
  def loop do
    receive do
      {event, node} -> on(event, node)
    after
      @time ->
        :global.send(LeaderLogic.current_leader(), {:PING, Node.self()})
        NodeData.print_nodes()
    end

    loop()
  end

  #
  # Default erlang monitor_nodes events
  def on(:nodeup, node) do
    IO.puts("Node connected: #{node}\n")
    :global.sync()

    if node > LeaderLogic.current_leader(), do: Election.start()
  end

  def on(:nodedown, node) do
    IO.puts("Node disconnected: #{node}\n")

    if node == LeaderLogic.current_leader(), do: Election.start()
  end

  # Only leader recieves PING message
  def on(:PING, node), do: :global.send(node, {:FINETHANKS, Node.self()})

  def on(:FINETHANKS, node) do
    IO.puts("Current leader: " <> to_string(node) <> "\n")
  end

  #
  # Election events used during running system
  def on(:ALIVE?, node) do
    :global.send(node, {:FINETHANKS, Node.self()})

    if Node.self() > node, do: Election.start()
  end

  def on(:IAMTHEKING, node) do
    if Node.self() > node do
      Election.start()
    else
      LeaderLogic.update_leader(node)
    end
  end
end
