# MiniDistributed

This is project implementing distributed bully algorithm on local network.

At the bottom of the page you can find links with source resource ideas on
which this project relies.

## Assumptions with selected design

That whole system runs on one machine network. That the node indentifires are
stored inside node name and are converted to IDs.

## Setup

Code was developed on setup below installed through [asdf](https://github.com/asdf-vm/asdf)

```text
Erlang/OTP 21 [erts-10.2.3] [source] [64-bit] [smp:8:8] [ds:8:8:10] [async-threads:1] [hipe]

Elixir 1.7.4 (compiled with Erlang/OTP 19)
```

## Run

First compile

```bash
mix compile
```

To run first node with id `1` use `--sname 1`.

Example (run from the root of project):

```bash
iex --sname 1 -S mix

# And then inside iex shell start first node
iex(1@localhost)1> MiniDistributed.start
```

Also the connect command with started id also works.

```elixir
MiniDistributed.connect(1)
```

Inside another terminal session spawn the another node with id 2.

```bash
iex --sname 2 -S mix

# And then inside iex shell connect to node with id 1
iex(1@localhost)1> MiniDistributed.connect(1)
```

You can connect N times with just connecting to any working node in network
providing its id.

For two running machines you should see in each shell session:

```bash
List of available nodes:
1@machineName
2@machineName
Current leader: 2@machineName
```

## Sources of code snippets

Along with [Wikipedia Bully algorithm](https://en.wikipedia.org/wiki/Bully_algorithm).

This solution was based and tweaked on code snippet
[mccraveiro bully-algorithm](https://github.com/mccraveiro/bully-algorithm/blob/master/node.exs)

Also the following sources were used in creation this project:

- [Djo leader_election](https://github.com/Djo/leader_election)
- [Medium How to test Elixir Cluster of nodes using slaves](https://medium.com/@lorenzo_sinisi/how-to-test-elixir-cluster-of-nodes-using-slaves-69e59a77ec3f)

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/mini_distributed](https://hexdocs.pm/mini_distributed).
